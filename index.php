<?php require_once('auth.php'); require_auth(); 

$selected_year = "2018";
$selected_month = "1";

$mysqli = new mysqli("localhost", "u_tomatinaN", "wfn4XunG", "tomatinas" );
if ($mysqli->connect_errno) {
    echo "Не удалось подключиться к MySQL: " . $mysqli->connect_error;
}

$res = $mysqli->query("SELECT COUNT(*) as count FROM analytics_orders WHERE  MONTH(analytics_orders.createdTime) <= ". $selected_month );
$row = $res->fetch_assoc();
$orders_in_selected_year = $row['count'];

$res = $mysqli->query("SELECT COUNT(*) as count FROM analytics_orders WHERE MONTH(analytics_orders.createdTime) = ". $selected_month );
$row = $res->fetch_assoc();
$orders_in_selected_month = $row['count'];


$selected_year = "2018";
$selected_month = "2";

$res = $mysqli->query("SELECT COUNT(*) as count FROM analytics_orders WHERE  MONTH(analytics_orders.createdTime) <= ". $selected_month );
$row = $res->fetch_assoc();
$orders_in_selected_year_2 = $row['count'];

$res = $mysqli->query("SELECT COUNT(*) as count FROM analytics_orders WHERE MONTH(analytics_orders.createdTime) = ". $selected_month );
$row = $res->fetch_assoc();
$orders_in_month_7 = $row['count'];




//echo $row['count'];
//die();


?>

<html><head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- PAGE settings -->
  <link rel="icon" href="ico">
  <title>Tomatina stats</title>
  <meta name="description" content="Tomatina stats">
  <meta name="keywords" content="Tomatina stats">
  <!-- CSS dependencies -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
  <link rel="stylesheet" href="theme.css">
  <!-- Script: Make my navbar transparent when the document is scrolled to top -->
  <script src="https://d33wubrfki0l68.cloudfront.net/js/12e3ca424c3c55e3d306e6423ed9e81f260dd657/aquamarine/js/navbar-ontop.js"></script>
  <!-- Script: Animated entrance -->
  <script src="https://d33wubrfki0l68.cloudfront.net/js/fe6311b3c294cba469a3939f21603640522c41e5/aquamarine/js/animate-in.js"></script>
</head><body>
  <!-- Navbar -->
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <div class="container">
        <a class="navbar-brand" href="#">Томатина</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar2SupportedContent" aria-controls="navbar2SupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
        <div class="collapse navbar-collapse text-center justify-content-end" id="navbar2SupportedContent">
            <a class="btn navbar-btn text-white mx-2 btn-primary" href="http://tomatina.ua">Сайт</a>
            <ul class="navbar-nav">
                <li class="nav-item mx-2">
                    <a class="nav-link" href="http://tomatina.amocrm.ru">AmoCRM</a>
                </li>
            </ul>
        </div>
    </div>
</nav>  
  <!-- Cover -->
<div class="pt-5 bg-primary">
    <div class="container mt-5 pt-5">
        <div class="row">
            <div class="col-md-12 my-5 text-lg-left text-center align-self-center">
                <h1 class="display-1">Статистика <?php echo date("Y");?></h1>

            </div>

        </div>
    </div>
</div>
  <!-- Article style section -->
<div class="py-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
				<ul class="nav nav-pills">
					<li class="nav-item"><a href="2017" class="btn btn-outline-primary m-2" data-toggle="pill">2017</a></li>
					<li class="nav-item"><a href="2018" class="btn active btn-primary m-2" data-toggle="pill">2018</a></li>
					<li class="nav-item"><a href="2019" class="btn disabled btn-outline-primary m-2" data-toggle="pill">2019</a></li>
					
				</ul>			
                <ul class="nav nav-pills">
                    <li class="nav-item">
                        <a href="" class="active nav-link" data-toggle="pill" data-target="#tabone">Янв</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="" data-toggle="pill" data-target="#tabtwo">Фев</a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link" data-toggle="pill" data-target="#tabthree">Мар</a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link" data-toggle="pill" data-target="#tabfour">Апр</a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link" data-toggle="pill" data-target="#tabfive">Май</a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link" data-toggle="pill" data-target="#tabsix">Июн</a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link" data-toggle="pill" data-target="#tabseven">Июл</a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link" data-toggle="pill" data-target="#tabeight">Авг</a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link" data-toggle="pill" data-target="#tabnine">Сен</a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link" data-toggle="pill" data-target="#tabten">Окт</a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link" data-toggle="pill" data-target="#tabeleven">Ноя</a>
                    </li>
                    <li class="nav-item">
                        <a href="" class="nav-link" data-toggle="pill" data-target="#tabtwelve">Дек</a>
                    </li>

                </ul>
                <div class="tab-content mt-2">
                    <div class="tab-pane fade show active" id="tabone" role="tabpanel">
                        <h3>Статистика за январь</h3>
                        <p class="">Всего заказов с начала года:&nbsp;<?=$orders_in_selected_year?><br>Всего заказов в январе:&nbsp;<?=$orders_in_selected_month?><br>Всего уникальных покупателей:&nbsp;<br>Уникальных покупателей в январе:&nbsp;<br>Покупатели делали покупку и зарегистрированы не в январе&nbsp;<br>Покупатели
                            делали покупку и зарегистрированы в январе&nbsp;<br>Отток в этом месяце (не покупали более месяца):&nbsp;<br>Вернулись из оттока (купили в этом месяце, предыдущая покупка была более 30 дней назад):
                        </p>
                    </div>
                    <div class="tab-pane fade" id="tabtwo" role="tabpanel">
                        <h3>Статистика за февраль</h3>
                        <p class="">Всего заказов с начала года:&nbsp;<?=$orders_in_selected_year_2?><br>Всего заказов в январе:&nbsp;<?=$orders_in_month_7?><br>Всего уникальных покупателей:&nbsp;<br>Уникальных покупателей в январе:&nbsp;<br>Покупатели делали покупку и зарегистрированы не в январе&nbsp;<br>Покупатели
                            делали покупку и зарегистрированы в январе&nbsp;<br>Отток в этом месяце (не покупали более месяца):&nbsp;<br>Вернулись из оттока (купили в этом месяце, предыдущая покупка была более 30 дней назад):
                        </p>
                    </div>
                    <div class="tab-pane fade" id="tabthree" role="tabpanel">
                        <h3>Статистика за март</h3>
                        <p class="">Всего заказов с начала года:&nbsp;<br>Всего заказов в январе:&nbsp;<br>Всего уникальных покупателей:&nbsp;<br>Уникальных покупателей в январе:&nbsp;<br>Покупатели делали покупку и зарегистрированы не в январе&nbsp;<br>Покупатели
                            делали покупку и зарегистрированы в январе&nbsp;<br>Отток в этом месяце (не покупали более месяца):&nbsp;<br>Вернулись из оттока (купили в этом месяце, предыдущая покупка была более 30 дней назад):
                        </p>
                    </div>
                    <div class="tab-pane fade" id="tabfour" role="tabpanel">
                        <h3>Статистика за апрель</h3>
                        <p class="">Всего заказов с начала года:&nbsp;<br>Всего заказов в январе:&nbsp;<br>Всего уникальных покупателей:&nbsp;<br>Уникальных покупателей в январе:&nbsp;<br>Покупатели делали покупку и зарегистрированы не в январе&nbsp;<br>Покупатели
                            делали покупку и зарегистрированы в январе&nbsp;<br>Отток в этом месяце (не покупали более месяца):&nbsp;<br>Вернулись из оттока (купили в этом месяце, предыдущая покупка была более 30 дней назад):
                        </p>
                    </div>
                    <div class="tab-pane fade" id="tabfive" role="tabpanel">
                        <h3>Статистика за май</h3>
                        <p class="">Всего заказов с начала года:&nbsp;<br>Всего заказов в январе:&nbsp;<br>Всего уникальных покупателей:&nbsp;<br>Уникальных покупателей в январе:&nbsp;<br>Покупатели делали покупку и зарегистрированы не в январе&nbsp;<br>Покупатели
                            делали покупку и зарегистрированы в январе&nbsp;<br>Отток в этом месяце (не покупали более месяца):&nbsp;<br>Вернулись из оттока (купили в этом месяце, предыдущая покупка была более 30 дней назад):
                        </p>
                    </div>
                    <div class="tab-pane fade" id="tabsix" role="tabpanel">
                        <h3>Статистика за июнь</h3>
                        <p class="">Всего заказов с начала года:&nbsp;<br>Всего заказов в январе:&nbsp;<br>Всего уникальных покупателей:&nbsp;<br>Уникальных покупателей в январе:&nbsp;<br>Покупатели делали покупку и зарегистрированы не в январе&nbsp;<br>Покупатели
                            делали покупку и зарегистрированы в январе&nbsp;<br>Отток в этом месяце (не покупали более месяца):&nbsp;<br>Вернулись из оттока (купили в этом месяце, предыдущая покупка была более 30 дней назад):
                        </p>
                    </div>
                    <div class="tab-pane fade" id="tabseven" role="tabpanel">
                        <h3>Статистика за июль</h3>
                        <p class="">Всего заказов с начала года:&nbsp;<br>Всего заказов в январе:&nbsp;<br>Всего уникальных покупателей:&nbsp;<br>Уникальных покупателей в январе:&nbsp;<br>Покупатели делали покупку и зарегистрированы не в январе&nbsp;<br>Покупатели
                            делали покупку и зарегистрированы в январе&nbsp;<br>Отток в этом месяце (не покупали более месяца):&nbsp;<br>Вернулись из оттока (купили в этом месяце, предыдущая покупка была более 30 дней назад):
                        </p>
                    </div>
                    <div class="tab-pane fade" id="tabeight" role="tabpanel">
                        <h3>Статистика за август</h3>
                        <p class="">Всего заказов с начала года:&nbsp;<br>Всего заказов в январе:&nbsp;<br>Всего уникальных покупателей:&nbsp;<br>Уникальных покупателей в январе:&nbsp;<br>Покупатели делали покупку и зарегистрированы не в январе&nbsp;<br>Покупатели
                            делали покупку и зарегистрированы в январе&nbsp;<br>Отток в этом месяце (не покупали более месяца):&nbsp;<br>Вернулись из оттока (купили в этом месяце, предыдущая покупка была более 30 дней назад):
                        </p>
                    </div>
                    <div class="tab-pane fade" id="tabnine" role="tabpanel">
                        <h3>Статистика за сентябрь</h3>
                        <p class="">Всего заказов с начала года:&nbsp;<br>Всего заказов в январе:&nbsp;<br>Всего уникальных покупателей:&nbsp;<br>Уникальных покупателей в январе:&nbsp;<br>Покупатели делали покупку и зарегистрированы не в январе&nbsp;<br>Покупатели
                            делали покупку и зарегистрированы в январе&nbsp;<br>Отток в этом месяце (не покупали более месяца):&nbsp;<br>Вернулись из оттока (купили в этом месяце, предыдущая покупка была более 30 дней назад):
                        </p>
                    </div>
                    <div class="tab-pane fade" id="tabten" role="tabpanel">
                        <h3>Статистика за октябрь</h3>
                        <p class="">Всего заказов с начала года:&nbsp;<br>Всего заказов в январе:&nbsp;<br>Всего уникальных покупателей:&nbsp;<br>Уникальных покупателей в январе:&nbsp;<br>Покупатели делали покупку и зарегистрированы не в январе&nbsp;<br>Покупатели
                            делали покупку и зарегистрированы в январе&nbsp;<br>Отток в этом месяце (не покупали более месяца):&nbsp;<br>Вернулись из оттока (купили в этом месяце, предыдущая покупка была более 30 дней назад):
                        </p>
                    </div>
                    <div class="tab-pane fade" id="tabeleven" role="tabpanel">
                        <h3>Статистика за ноябрь</h3>
                        <p class="">Всего заказов с начала года:&nbsp;<br>Всего заказов в январе:&nbsp;<br>Всего уникальных покупателей:&nbsp;<br>Уникальных покупателей в январе:&nbsp;<br>Покупатели делали покупку и зарегистрированы не в январе&nbsp;<br>Покупатели
                            делали покупку и зарегистрированы в январе&nbsp;<br>Отток в этом месяце (не покупали более месяца):&nbsp;<br>Вернулись из оттока (купили в этом месяце, предыдущая покупка была более 30 дней назад):
                        </p>
                    </div>
                    <div class="tab-pane fade" id="tabtwelve" role="tabpanel">
                        <h3>Статистика за декабрь</h3>
                        <p class="">Всего заказов с начала года:&nbsp;<br>Всего заказов в январе:&nbsp;<br>Всего уникальных покупателей:&nbsp;<br>Уникальных покупателей в январе:&nbsp;<br>Покупатели делали покупку и зарегистрированы не в январе&nbsp;<br>Покупатели
                            делали покупку и зарегистрированы в январе&nbsp;<br>Отток в этом месяце (не покупали более месяца):&nbsp;<br>Вернулись из оттока (купили в этом месяце, предыдущая покупка была более 30 дней назад):
                        </p>
                    </div>
				</div>
            </div>
        </div>
    </div>
</div>
	
	<div class="py-5"></div>
  
  
  <!-- Footer -->
<div class="pt-5 bg-dark">
    <div class="container">
        <div class="row">
            <div class="col-md-9"></div>
            <div class="col-4 col-md-1 align-self-center my-3">
                <a href="https://www.facebook.com" target="blank"><i class="fa fa-fw fa-facebook fa-3x text-white"></i></a>
            </div>
            <div class="col-4 col-md-1 align-self-center my-3">
                <a href="https://twitter.com" target="blank"><i class="fa fa-fw fa-twitter fa-3x text-white"></i></a>
            </div>
            <div class="col-4 col-md-1 align-self-center my-3">
                <a href="https://www.instagram.com" target="blank"><i class="fa fa-fw fa-instagram fa-3x text-white"></i></a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 my-3 text-center"></div>
        </div>
    </div>
</div>  
  <!-- JavaScript dependencies -->
  <script src="http://code.jquery.com/jquery-3.2.1.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <!-- Script: Smooth scrolling between anchors in the same page -->
  <script src="https://d33wubrfki0l68.cloudfront.net/js/26a3094ac9c5e8d045f102e8929a6db58b3bbd83/aquamarine/js/smooth-scroll.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->

</body>
</html>